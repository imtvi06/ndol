#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Aug 18 12:04:56 2019

@author: javier
"""

import os, fnmatch, openpyxl


def finder(pattern, root=os.curdir):
    '''Generator expression that locates all files matching **pattern**
    argument in and inside dir **root** argument.
    Args:
        pattern (str): specified pattern.
        root (str)   : path where to begin search.
    '''
    for path, dirs, files in os.walk(os.path.abspath(root)):
        for filename in fnmatch.filter(files, pattern):
            yield os.path.join(path, filename)


# ------ Generando Excel y planilla ------ #
#wb = openpyxl.Workbook()
#wb.get_active_sheet()
#sheet = wb.get_sheet_by_name('Sheet')
#sheet['D4'] = 'TD-DFT'
#sheet['F4'] = 'TD-HF'
#sheet['H4'] = 'CIS'
#sheet['C5'] = 'Compound'
#sheet['D5'] = 'energy (eV)'
#sheet['E5'] = 'wavelengths (nm)'
#sheet['F5'] = 'energy (eV)'
#sheet['G5'] = 'wavelengths (nm)'
#sheet['H5'] = 'energy (eV)'
#sheet['I5'] = 'wavelengths (nm)'
H = 5
# -------------------------------------------------------------------------- #

out_files = list(finder('*.log'))
for i in out_files:
    out_text = []
    beginning = []
    end = []
    mulliken = []
    mulliken_end = []
    electroestatic = []
    hirshfeld = []
    hirshfeld_end = []
    with open(i, 'r') as file:
        for line in file:
            out_text.append(line)
    for j in out_text:
        if j.startswith(' Cite this work as:'):
            beginning.append(out_text.index(j))
            out_text.remove(j)
        if j.startswith(' Normal termination'):
            end.append(out_text.index(j))
        if (j.startswith(' Mulliken atomic charges:') or j.startswith(' Mulliken charges:')):
            mulliken.append(out_text.index(j))
            out_text.remove(j)
        if (j.startswith(' Sum of Mulliken atomic charges =') or j.startswith(' Sum of Mulliken charges =')):
            mulliken_end.append(out_text.index(j))
            out_text.remove(j)
        if j.startswith(' Charges from ESP '):
            electroestatic.append(out_text.index(j))
            out_text.remove(j)
        if j.startswith(' Hirshfeld charges, spin densities,'):
            hirshfeld.append(out_text.index(j))
            out_text.remove(j)
    del mulliken[0]
    del mulliken_end[0]
    if (electroestatic != []):
        del electroestatic[0]
    if (hirshfeld != []):
        del hirshfeld[0]
    information = os.path.split(i)
    name = information[1]
    address = information[0]
    name = name[0: name.find('.')]
    if (len(end) != len(beginning)):
        with open ('Informe_de_error', 'a') as error_report:
            error_report.write('  ' + name + '\n')
        continue

# ------ Generando fichero .xyz para Jmol del estado base ------ #
    mulliken_ground_state = out_text[mulliken[0]:mulliken_end[0]]
    mulliken_ground_state.remove(mulliken_ground_state[0])
    cant_atoms = len(mulliken_ground_state)
    ground_state = out_text[beginning[0]:end[0]]
#    del ground_state[0:ground_state.index(' Optimization completed.\n')]
    coordenadas = ground_state.index('                          Input orientation:                          \n')
    xyz = ground_state[(coordenadas + 5):(coordenadas + cant_atoms + 5)]
    xyz_format = []
    for (k, l) in zip(xyz, mulliken_ground_state):
        xyz_format.append(' ' + l[8] + '   ' + k[36:69] + '    ' + l[12:20] + '\n')
    with open(address + '/' + name + '_Mulliken_S0.xyz', 'w') as ground_state_file:
        ground_state_file.write('    ' + str(cant_atoms) + '\n')
        ground_state_file.write('jmolscript:isosurface resolution 10 molecular 0.0 map MEP; background white; color isosurface range -0.1 0.1;   isosurface translucent\n')
        for m in xyz_format:
            ground_state_file.write(m)
    xyz_format = []
    if (electroestatic != []):
        electroestatic_ground_state = out_text[electroestatic[0] + 2:electroestatic[0] + 2 + cant_atoms]
        for (k, l) in zip(xyz, electroestatic_ground_state):
            xyz_format.append(' ' + l[8] + '   ' + k[36:69] + '    ' + l[12:20] + '\n')
        with open(address + '/' + name + '_EPS_S0.xyz', 'w') as ground_state_file:
            ground_state_file.write('    ' + str(cant_atoms) + '\n')
            ground_state_file.write('jmolscript:isosurface resolution 10 molecular 0.0 map MEP; background white; color isosurface range -0.1 0.1;   isosurface translucent\n')
            for m in xyz_format:
                ground_state_file.write(m)
    xyz_format = []
    if (hirshfeld != []):
        hirshfeld_ground_state = out_text[hirshfeld[0] + 1:hirshfeld[0] + 1 + cant_atoms]
        for (k, l) in zip(xyz, hirshfeld_ground_state):
            xyz_format.append(' ' + l.split()[1] + '   ' + k[36:69] + '    ' + l.split()[2] + '\n')
        with open(address + '/' + name + '_H_S0.xyz', 'w') as ground_state_file:
            ground_state_file.write('    ' + str(cant_atoms) + '\n')
            ground_state_file.write('jmolscript:isosurface resolution 10 molecular 0.0 map MEP; background white; color isosurface range -0.1 0.1;   isosurface translucent\n')
            for m in xyz_format:
                ground_state_file.write(m)

# -------------------------------------------------------------------------- #

# ------ Extrayendo informacion de estados excitados ------ #
    del mulliken
    del mulliken_end
    del end[0]
    del beginning[0]
    if (electroestatic != []):
        del electroestatic
    if (hirshfeld != []):
        del hirshfeld
    for n in range(len(beginning)):
        h = H
        excited_state = out_text[beginning[n]:end[n]]
        for o in excited_state:
            if o.startswith(' Redundant internal coordinates'):
                calculo = excited_state.index(o) - 2
                break
        if (' Structure from the checkpoint file' in excited_state[calculo]):
            calculo = calculo - 2
        if not ('cc-pVTZ' in excited_state[calculo]):
            calculo = calculo - 1
        calculo = excited_state[calculo].split()
        for o in calculo:
            if (('TD' in o) or ('CIS' in o)):
                temp1 = o
        calculo = temp1
        if ('CIS' in calculo):
            calculo = 'CIS'
        elif ('HF' in calculo):
            calculo = 'TD-HF'
        elif ('PBE' in calculo):
            calculo = 'TD-DFT'
        excitation_energies = []
        for o in excited_state:
            if o.startswith(' Excited State '):
                excitation_energies.append(o)
            if o.startswith(' Mulliken charges with hydrogens summed into '):
                mulliken = excited_state.index(o)-cant_atoms
            if (o.startswith(' Fitting point')):
                electroestatic = excited_state.index(o) + 3
            if (o.startswith(' Hirshfeld charges with hydrogens summed into heavy atoms:')):
                hirshfeld = excited_state.index(o) - cant_atoms - 1
        energy_excited_state = []
        wavelengths = []
        oscillator_strengths = []
        for s in excitation_energies:
            temporal = s.split()
            energy_excited_state.append(temporal[4])
            wavelengths.append(temporal[6])
            oscillator_strengths.append(temporal[8])
# -------------------------------------------------------------------------- #

# ------ Generando ficheros .xyz para Jmol de estados excitados ------ #
        xyz_format_excited_state = []
        mulliken_excited_state = excited_state[mulliken:mulliken + cant_atoms]
        for (p, q) in zip(xyz, mulliken_excited_state):
            xyz_format_excited_state.append(' ' + q[8] + '   ' + p[36:69] + '    ' + q[12:22])
        with open(address + '/' + name + '_' + calculo + '_Mulliken_S1.xyz', 'w') as excited_state_file:
            excited_state_file.write('    ' + str(cant_atoms) + '\n')
            excited_state_file.write('jmolscript:isosurface resolution 10 molecular 0.0 map MEP; background white; color isosurface range -0.1 0.1;   isosurface translucent\n')
            for r in xyz_format_excited_state:
                excited_state_file.write(r)
        xyz_format_excited_state = []
        difference = []
        for (p, q) in zip(mulliken_ground_state, mulliken_excited_state):
            a = p.split()
            a = float(a[2])
            b = q.split()
            b = float(b[2])
            difference.append(round(b-a, 8))
        for (p, q, x) in zip(xyz, difference, mulliken_excited_state):
            xyz_format_excited_state.append(' ' + x[8] + '    ' + p[36:69] + '    ' + str(q))
        with open(address + '/' + name + '_' + calculo + '_dMulliken_S1.xyz', 'w') as excited_state_file:
            excited_state_file.write('    ' + str(cant_atoms) + '\n')
            excited_state_file.write('jmolscript:isosurface resolution 10 molecular 0.0 map MEP; background white; color isosurface range -0.05 0.05;   isosurface translucent\n')
            for r in xyz_format_excited_state:
                excited_state_file.write(r + '\n')
        if (electroestatic != []):
            xyz_format_excited_state = []
            electroestatic_excited_state = excited_state[electroestatic : electroestatic + cant_atoms]
            for (p, q) in zip(xyz, electroestatic_excited_state):
                xyz_format_excited_state.append(' ' + q[8] + '   ' + p[36:69] + '    ' + q[12:22])
            with open(address + '/' + name + '_' + calculo + '_EPS_S1.xyz', 'w') as excited_state_file:
                excited_state_file.write('    ' + str(cant_atoms) + '\n')
                excited_state_file.write('jmolscript:isosurface resolution 10 molecular 0.0 map MEP; background white; color isosurface range -0.1 0.1;   isosurface translucent\n')
                for r in xyz_format_excited_state:
                    excited_state_file.write(r)
            xyz_format_excited_state = []
            difference = []
            for (p, q) in zip(electroestatic_ground_state, electroestatic_excited_state):
                a = p.split()
                a = float(a[2])
                b = q.split()
                b = float(b[2])
                difference.append(round(b-a, 8))
            for (p, q, x) in zip(xyz, difference, electroestatic_excited_state):
                xyz_format_excited_state.append(' ' + x[8] + '    ' + p[36:69] + '    ' + str(q))
            with open(address + '/' + name + '_' + calculo + '_dEPS_S1.xyz', 'w') as excited_state_file:
                excited_state_file.write('    ' + str(cant_atoms) + '\n')
                excited_state_file.write('jmolscript:isosurface resolution 10 molecular 0.0 map MEP; background white; color isosurface range -0.05 0.05;   isosurface translucent\n')
                for r in xyz_format_excited_state:
                    excited_state_file.write(r + '\n')
        if (hirshfeld != []):
            xyz_format_excited_state = []
            hirshfeld_excited_state = excited_state[hirshfeld : hirshfeld + cant_atoms]
            for (p, q) in zip(xyz, hirshfeld_excited_state):
                xyz_format_excited_state.append(' ' + q.split()[1] + '   ' + p[36:69] + '    ' + q.split()[2] + '\n')
            with open(address + '/' + name + '_' + calculo + '_H_S1.xyz', 'w') as excited_state_file:
                excited_state_file.write('    ' + str(cant_atoms) + '\n')
                excited_state_file.write('jmolscript:isosurface resolution 10 molecular 0.0 map MEP; background white; color isosurface range -0.1 0.1;   isosurface translucent\n')
                for r in xyz_format_excited_state:
                    excited_state_file.write(r)
            xyz_format_excited_state = []
            difference = []
            for (p, q) in zip(hirshfeld_ground_state, hirshfeld_excited_state):
                a = p.split()
                a = float(a[2])
                b = q.split()
                b = float(b[2])
                difference.append(round(b-a, 8))
            for (p, q, x) in zip(xyz, difference, hirshfeld_excited_state):
                xyz_format_excited_state.append(' ' + x[8] + '    ' + p[36:69] + '    ' + str(q))
            with open(address + '/' + name + '_' + calculo + '_dH_S1.xyz', 'w') as excited_state_file:
                excited_state_file.write('    ' + str(cant_atoms) + '\n')
                excited_state_file.write('jmolscript:isosurface resolution 10 molecular 0.0 map MEP; background white; color isosurface range -0.05 0.05;   isosurface translucent\n')
                for r in xyz_format_excited_state:
                    excited_state_file.write(r + '\n')

# -------------------------------------------------------------------------- #

# ---- Copiando en Excel las energias y demas de los estados excitados ---- #
#        sheet['C' + str(h)] = name
#        if (('MP2' in address) or ('MP2' in name)):
#            sheet['B' + str(h)] = 'MP2'
#        elif (('PBE' in address) or ('PBE0' in name)):
#            sheet['B' + str(h)] = 'PBE0'
#        if ('CIS' in calculo):
#            for (t, u, v) in zip(energy_excited_state, wavelengths, #oscillator_strengths):
#                sheet['H' + str(h)] = str(round(float(t), 2)) + '  [' + #v[v.find('=') + 1:] + ']'
#                sheet['I' + str(h)] = round((float(u)))
#                h = h + 1
#        elif ('TD-HF' in calculo):
#            for (t, u, v) in zip(energy_excited_state, wavelengths, #oscillator_strengths):
#                sheet['F' + str(h)] = str(round(float(t), 2)) + '  [' + #v[v.find('=') + 1:] + ']'
#                sheet['G' + str(h)] = round((float(u)))
#                h = h + 1
#        elif ('TD-DFT' in calculo):
#            for (t, u, v) in zip(energy_excited_state, wavelengths, #oscillator_strengths):
#                sheet['D' + str(h)] = str(round(float(t), 2)) + '  [' + #v[v.find('=') + 1:] + ']'
#                sheet['E' + str(h)] = round((float(u)))
#                h = h + 1
#    H = H + 1 + len(energy_excited_state)
#wb.save('Resumen_General.xlsx')
