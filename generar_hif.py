#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 17 23:36:21 2019

@author: javier
"""


import os, fnmatch


def finder(pattern, root=os.curdir):
    '''Generator expression that locates all files matching **pattern**
    argument in and inside dir **root** argument.
    Args:
        pattern (str): specified pattern.
        root (str)   : path where to begin search.
    '''
    for path, dirs, files in os.walk(os.path.abspath(root)):
        for filename in fnmatch.filter(files, pattern):
            yield os.path.join(path, filename)

com_files = list(finder('*.com'))
sub_calculo = ['PBE1PBE TD', 'TD', 'CIS']
for i in com_files:
    information = []
    with open(i, 'r') as file:
        information = file.readlines()
    del information[15:]
    address_name = os.path.split(i)
    address = address_name[0]
    name = address_name[1]
    name = name[0:name.find('.')]
    name = name + '_H.com'
    route_card = information[3]
    for j in information:
        if j.startswith('%chk'):
            chk = j
        if j.startswith('%nproc=12'):
            nproc = j
        if j.startswith('%mem=1GW'):
            mem = j
    with open(address + '/' + name, 'w') as new_file:
        new_file.write(chk)
        new_file.write(nproc)
        new_file.write(mem)
        if ('MP2' in route_card):
            new_file.write('# cc-pVTZ MP2 IOp(6/79=1) geom=checkpoint guess=read\n')
        elif ('PBE1PBE' in route_card):
            new_file.write('# cc-pVTZ PBE1PBE IOp(6/79=1) geom=checkpoint guess=read\n')
        new_file.write('\n' + name[0:name.find('.')])
        new_file.write('\n\n0 1\n')
        for k in sub_calculo:
            new_file.write('\n--Link1--\n')
            new_file.write(chk)
            new_file.write(nproc)
            new_file.write(mem)
            new_file.write('# cc-pVTZ ' + k + '(nstates=6,root=1,singlets) IOp(6/79=1) density=current geom=checkpoint\n')
            if (k == sub_calculo[0]):
                new_file.write('\n' + name[0:name.find('.')] + '-TD-PBE0\n')
            elif (k == sub_calculo[1]):
                new_file.write('\n' + name[0:name.find('.')] + '-TD-HF\n')
            elif (k == sub_calculo[2]):
                new_file.write('\n' + name[0:name.find('.')] + '-CIS\n')
            new_file.write('\n0 1\n')
        new_file.write('\n\n')


